import { useState } from "react";

import BMIResult from "../component/BMIResult";

 function BMICalPage (proops){

    const [Name, setName ] = useState("");
    const [Bmi, setBmi ] = useState("");
    const [Result, setResult ] = useState("");

    const [height, setheight ] = useState("");
    const [weight, setweight ] = useState("");

    function calculateBMI (){
        let h =parseFloat(height);
        let w =parseFloat(weight);
        let bmi = w/(h*h);
        setBmi(bmi);

        if(Bmi > 25){
            setResult("สุดยอด")
            
        }
       
        else{
            setResult("อ่อนมาก ๆ")
        }
    }
    return(
     <div align="left">
         <div align="center">
             ยินดีต้อนรับสู่เว็บคำนวณ BMI
             <hr/>

                คุณชื่อ: <input type="text"
                                value={Name}
                                onChange={(e) => {setName(e.target.value);} } /> <br/>
                สูง: <input type="text" 
                            value = {height}
                            onChange={(e) => {setheight(e.target.value);} }/><br/>
                หนัก: <input type="text" 
                            value = {weight}
                            onChange={(e) => {setweight(e.target.value);} }/><br/>

                <button onClick={() =>{ calculateBMI() } }> Calculate </button>
            { Bmi != 0 &&
            <div>
                <hr />
             นี่คือผลการคำนวณ:
               
            
            <BMIResult 
            name = {Name}
            bmi = {Bmi}
            result = {Result}
            />
            </div>
            }
             
            
         </div>
     </div>
     );
 }

 export default BMICalPage