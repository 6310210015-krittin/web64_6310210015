import { useState } from "react";
import Button from '@mui/material/Button';

import BMIResult from "../component/BMIResult";
import { Grid, Typography, Box, Container } from "@mui/material";


 function BMICalPage (props){

    const [Name, setName ] = useState("");
    const [Bmi, setBmi ] = useState("");
    const [Result, setResult ] = useState("");

    const [height, setheight ] = useState("");
    const [weight, setweight ] = useState("");

    function calculateBMI (){
        let h =parseFloat(height);
        let w =parseFloat(weight);
        let bmi = w/(h*h);
        setBmi(bmi);

        if(Bmi > 25){
            setResult("สุดยอด")
            
        }
       
        else{
            setResult("อ่อนมาก ๆ")
        }
    }
    return(
    <Container maxwidth='lg'>
    <Grid container spacing={2} sx = { {marginTop : "10px" }}>
        <Grid item xs={12}>
        <Typography variant="h5">ยินดีต้อนรับสู่เว็บคำนวณ BMI</Typography>
        </Grid>
  <Grid item xs={8} >
    <Box sx={{ textAlign : 'left'}} >   
            คุณชื่อ: <input type="text"
                value={Name}
                onChange={(e) => {setName(e.target.value);} } /> 
                <br />
                <br />
            สูง: <input type="text" 
                value = {height}
                onChange={(e) => {setheight(e.target.value);} }/><br/>
                <br />
            หนัก: <input type="text" 
                value = {weight}
                onChange={(e) => {setweight(e.target.value);} }/><br/>
                <br />
{/* <button onClick={() =>{ calculateBMI() } }> Calculate </button> */}
                <Button variant="contained">
                    Calculate
                </Button>
    </Box>
  </Grid>
  <Grid item xs={4}>
  { Bmi != 0 &&
            <div>
                <hr />
             นี่คือผลการคำนวณ:
               
            
            <BMIResult 
            name = {Name}
            bmi = {Bmi}
            result = {Result}
            />
            </div>
            }
             
  </Grid>
</Grid>
   </Container>
     );
}

 export default BMICalPage