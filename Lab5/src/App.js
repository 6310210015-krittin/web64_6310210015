
import './App.css';
import Header from './component/Header';
import AboutUsPage from './pages/AboutUsPage';
import BMICalPage from './pages/BMICalPage';
import {Route, Routes } from "react-router-dom";
import LuckyNumberPage from './pages/LuckyNumberPage';

function App() {
  return (
    <div className="App">
      <Header />
      <Routes>
          <Route path="about" element={
                <AboutUsPage />
              } />

       
          <Route path="/" element={
              <BMICalPage />
                }/>
          <Route path="/lucky" element={
              <LuckyNumberPage />
                }/>
      </Routes>
    </div>
  );
}

export default App;
